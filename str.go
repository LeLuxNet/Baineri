package baineri

import (
	"fmt"
	"sync"

	"github.com/llir/irutil"
	"github.com/llir/llvm/ir"
)

var tmp int
var tmpLock sync.Mutex

func tmpName() string {
	tmpLock.Lock()
	defer tmpLock.Unlock()

	name := fmt.Sprintf("tmp%d", tmp)
	tmp++
	return name
}

func String(m *ir.Module, s string) *ir.Global {
	return m.NewGlobalDef(tmpName(), irutil.NewCString(s))
}

package baineri

import (
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"github.com/llir/llvm/ir"
)

func Compile(m *ir.Module, w io.Writer) error {
	llc := exec.Command("llc")
	llc.Stdin = strings.NewReader(m.String())
	llc.Stderr = os.Stderr
	llcOut, err := llc.StdoutPipe()
	if err != nil {
		return err
	}
	defer llcOut.Close()

	gcc := exec.Command("gcc", "-xassembler", "-o", "/dev/stdout", "-")
	gcc.Stdin = llcOut
	gcc.Stdout = w
	gcc.Stderr = os.Stderr

	err = llc.Start()
	if err != nil {
		return err
	}

	return gcc.Run()
}

func Run(m *ir.Module) error {
	f, err := ioutil.TempFile("", "")
	if err != nil {
		return err
	}
	defer os.Remove(f.Name())

	err = Compile(m, f)
	if err != nil {
		return err
	}

	err = f.Close()
	if err != nil {
		return err
	}

	cmd := exec.Command(f.Name())
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

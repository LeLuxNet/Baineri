package baineri

import (
	"github.com/llir/llvm/ir/constant"
	"github.com/llir/llvm/ir/types"
)

var (
	Zero = constant.NewInt(types.I64, 0)
)

package baineri

import (
	"github.com/llir/llvm/ir"
	"github.com/llir/llvm/ir/types"
	"github.com/llir/llvm/ir/value"
)

func strPtr(b *ir.Block, val value.Value) value.Value {
	if global, ok := val.(*ir.Global); ok {
		return b.NewGetElementPtr(global.ContentType, global, Zero, Zero)
	}

	return val
}

func Printf(b *ir.Block, format value.Value, values ...value.Value) {
	printf := b.Parent.Parent.NewFunc("printf", types.I32, ir.NewParam("format", types.I8Ptr))
	printf.Sig.Variadic = true

	args := append([]value.Value{strPtr(b, format)}, values...)
	b.NewCall(printf, args...)
}

module lelux.net/baineri

go 1.16

require (
	github.com/llir/irutil v0.0.0-20211226215541-daa08657c5c1
	github.com/llir/llvm v0.3.5-0.20220221132528-0ad52b0f4472
	github.com/mewmew/float v0.0.0-20211212214546-4fe539893335 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/tools v0.1.10 // indirect
	golang.org/x/xerrors v0.0.0-20220517211312-f3a8303e98df // indirect
)
